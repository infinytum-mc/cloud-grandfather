package config

// Memory contains server statistics
type Memory struct {
	Cores    int
	UsedRAM  int64
	TotalRAM int64
}
