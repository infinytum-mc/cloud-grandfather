package config

// McServer information for the proxies to registe
type McServer struct {
	Host string
	Name string
	Port int
}
