package config

type Server struct {
	Host         string
	Name         string
	Port         int
	ServerTypeId int
}
