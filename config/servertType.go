package config

// ServerType defines all the Information which have a specific server type
type ServerType struct {
	ID        int
	Name      string
	Maps      []string
	MaxPlayer int
	Plugins   []string
}
